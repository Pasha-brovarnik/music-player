const trackImage = getElement('img');
const trackTitle = getElement('#title');
const trackArtist = getElement('#artist');
const music = getElement('audio');
const progressContainer = getElement('#progress-container');
const progress = getElement('#progress');
const currentTimeElm = getElement('#current-time');
const durationElm = getElement('#duration');
const prevBtn = getElement('#prev');
const playBtn = getElement('#play');
const nextBtn = getElement('#next');

function getElement(selector) {
	return document.querySelector(selector);
}

// Music
const tracks = [
	{
		trackArt: 'tech-house',
		trackName: 'alejandro-magana-tech-house-vibes',
		displayName: 'Tech house vides',
		artist: 'Alejandro Magana',
	},
	{
		trackArt: 'hip-hop',
		trackName: 'lily-j-hip-hop',
		displayName: 'Hip-Hop vides',
		artist: 'Lily J',
	},
	{
		trackArt: 'dream',
		trackName: 'michael-ramir-life-is-a-dream',
		displayName: 'Life is a dream',
		artist: 'Michael Ramir',
	},
	{
		trackArt: 'christmas',
		trackName: 'michael-ramir-a-very-happy-christmas',
		displayName: 'A very happy Christmas',
		artist: 'Michael Ramir',
	},
];

// Check if playing
let isPlaying = false;

// Play
function playTrack() {
	isPlaying = true;
	playBtn.classList.replace('fa-play', 'fa-pause');
	playBtn.setAttribute('title', 'Pause');
	music.play();
}

// Pause
function pauseTrack() {
	isPlaying = false;
	playBtn.classList.replace('fa-pause', 'fa-play');
	playBtn.setAttribute('title', 'Play');
	music.pause();
}

// Update dom with tracks
function loadTrack(track) {
	trackTitle.textContent = track.displayName;
	trackArtist.textContent = track.artist;
	music.src = `music/${track.trackName}.mp3`;
	trackImage.src = `img/${track.trackArt}.jpeg`;
}

// Current track
let trackIndex = 0;

// Next track
function nextTrack() {
	if (trackIndex >= tracks.length - 1) {
		pauseTrack();
		return;
	}
	trackIndex++;
	loadTrack(tracks[trackIndex]);
	playTrack();
}

// Previous track
function prevTrack() {
	if (trackIndex <= 0) {
		return;
	}
	trackIndex--;
	loadTrack(tracks[trackIndex]);
	playTrack();
}

// On load - select first track
loadTrack(tracks[trackIndex]);

// Update progress bar and time
function updateProgressBar({ srcElement: { duration, currentTime } }) {
	if (isPlaying) {
		// Update progress bar
		const progressPercent = (currentTime / duration) * 100;
		progress.style.width = `${progressPercent}%`;

		// Calculate display for duration
		const durationMinutes = Math.floor(duration / 60);
		let durationSeconds = Math.floor(duration % 60);

		if (durationSeconds < 10) {
			durationSeconds = `0${durationSeconds}`;
		}

		// Delay switch duration element to avoid NaN
		if (durationSeconds) {
			durationElm.textContent = `${durationMinutes}:${durationSeconds}`;
		}

		// Calculate currentTime
		const currentMinutes = Math.floor(currentTime / 60);
		let currentSeconds = Math.floor(currentTime % 60);

		if (currentSeconds < 10) {
			currentSeconds = `0${currentSeconds}`;
		}

		currentTimeElm.textContent = `${currentMinutes}:${currentSeconds}`;
	}
}

// Set Progress bar
function setProgressBar({ offsetX }) {
	const width = this.clientWidth;
	let { duration } = music;
	let newProgressPosition = (offsetX / width) * duration;
	const progressPercent = (newProgressPosition / duration) * 100;

	progress.style.width = `${progressPercent}%`;
	music.currentTime = newProgressPosition;
}

// Event listener
playBtn.addEventListener('click', () =>
	isPlaying ? pauseTrack() : playTrack()
);
prevBtn.addEventListener('click', prevTrack);
nextBtn.addEventListener('click', nextTrack);
music.addEventListener('ended', nextTrack);
music.addEventListener('timeupdate', updateProgressBar);
progressContainer.addEventListener('click', setProgressBar);
